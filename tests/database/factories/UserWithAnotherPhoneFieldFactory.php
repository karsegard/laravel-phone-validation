<?php

namespace KDA\Tests\Database\Factories;

use KDA\Tests\Models\UserWithAnotherPhoneField;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserWithAnotherPhoneFieldFactory extends Factory
{
    protected $model = UserWithAnotherPhoneField::class;

    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'email'=>$this->faker->email(),
            'password'=>$this->faker->word(),
            'home_phone'=>$this->faker->phoneNumber(),
            'work_phone'=>$this->faker->phoneNumber(),
            'home_phone_country'=>'us',
            'work_phone_country'=>'us'
        ];
    }
}
