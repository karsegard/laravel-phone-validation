<?php

namespace KDA\Tests\Models;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Laravel\PhoneValidation\Models\Traits\HasPhoneNumbers;
use KDA\Tests\Database\Factories\UserWithAnotherPhoneFieldFactory;
/**
 * @mixin IdeHelperUser
 */
class UserWithAnotherPhoneField extends Authenticatable 
{
   
    use Notifiable;
    use HasFactory;
    use HasPhoneNumbers;

    public  $phone_fields = ['home_phone','work_phone'];
    public $table ="users_alt";
    protected $fillable = ['name', 'email', 'password'];

    protected $searchableFields = ['*'];

    protected $hidden = [
        'password',
        'remember_token',
     
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

   
    /**
     * Add a mutator to ensure hashed passwords
     */
    public function setPasswordAttribute($value)
    {
        if (\Hash::needsRehash($value)) {
            //dd('hashed_password');
            $value = \Hash::make($value);
        } else {
            // dd('not hashed',$value);
        }
        $this->attributes['password'] = $value;
    }
    protected static function newFactory()
    {
        return  UserWithAnotherPhoneFieldFactory::new();
    }
}
