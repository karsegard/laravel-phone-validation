<?php

namespace KDA\Tests\Behat\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Support\Facades\Storage;
use KDA\Tests\TestCase;

/**
 * Defines application features from the specific context.
 */
class BaseContext extends TestCase implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        putenv('DB_CONNECTION='.env('BEHAT_DB_CONNECTION','mysql'));
        putenv('DB_DATABASE='.env('BEHAT_DB_DATABASE','testing'));
        putenv('DB_HOST='.env('BEHAT_DB_HOST','127.0.0.1'));
        putenv('DB_USERNAME='.env('BEHAT_DB_USERNAME','root'));
        putenv('DB_PASSWORD='.env('BEHAT_DB_PASSWORD','password'));
        $this->setUp();
    }


    /** @BeforeScenario */
    public function before(BeforeScenarioScope $scope)
    {
        $this->artisan('migrate:fresh');

        $this->app[Kernel::class]->setArtisan(null);
    }

    /** @AfterScenario */
    public function after(AfterScenarioScope $scope)
    {
        $this->artisan('migrate:rollback', ['--database' => 'mysql']);
    }

    protected function tearDown(): void
    {
      
        parent::tearDown();
    }

    public function setUp(): void
    {
        parent::setUp();
    }
}
