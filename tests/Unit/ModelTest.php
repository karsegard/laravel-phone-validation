<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Tests\Models\User;
use KDA\Tests\Models\UserWithAnotherPhoneField;
use KDA\Tests\TestCase;
use Propaganistas\LaravelPhone\PhoneNumber;

class ModelTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function model_created_null()
  {
    $user = User::factory()->create(['phone' => null]);
    /*$user = User::factory()->create();
    dump($user->phone,$user->phone_country,$user->phone_normalized,$user->phone_national);

    $user = User::factory()->create(['phone'=>'asdghasd','phone_country'=>'ch']);*/
    $this->assertNull($user->phone);
  }

  /** @test */
  function model_created_valid()
  {
    $user = User::factory()->create();
    $this->assertTrue($user->phone instanceof PhoneNumber);
  }


  /** @test */
  function model_try_invalid()
  {
    $this->expectException(\Propaganistas\LaravelPhone\Exceptions\NumberParseException::class );
    $user = User::factory()->create();
    $this->assertTrue($user->phone instanceof PhoneNumber);
    $user->phone="crapy value";
    $user->save();
  }


  /** @test */
  function model_field_try_invalid()
  {
    $this->expectException(\Propaganistas\LaravelPhone\Exceptions\NumberParseException::class );
    $user = UserWithAnotherPhoneField::factory()->create();
    $this->assertTrue($user->home_phone instanceof PhoneNumber);
    $user->home_phone="crapy value";
    $user->save();
  }


  /** @test */
  function model_created_from_existing_invalid()
  {
    \DB::insert('insert into users (id,name, email,password,phone,phone_country) values (?,?, ?,?,?,?)', [999, 'Dayle', 'dayle@example.com', 'test', 'crap_number', 'ch']);
    //$user = User::factory()->create(['phone'=>'asdghasd','phone_country'=>'ch']);
    $user = User::find(999);
    $this->assertNotNull($user);
    $this->assertEquals($user->phone, 'crap_number');

    $user->phone = '+41227771277';
    $user->save();
    $this->assertTrue($user->phone instanceof PhoneNumber);
    $this->assertNotNull($user->phone_national);
    $this->assertNotNull($user->phone_normalized);
    $this->assertEquals($user->phone_normalized,'+41227771277');
    $this->assertEquals($user->phone_national,'022 777 12 77');

    $this->assertEquals(1, User::wherePhone('022 777')->count());
    $this->assertEquals(1, User::wherePhone('22 777')->count());
  }



  /** @test */
  function model_different_field_created_null()
  {
    $user = UserWithAnotherPhoneField::factory()->create(['home_phone' => null]);
    $this->assertNull($user->home_phone);
    $this->assertNotNull($user->work_phone);
  }


  /** @test */
  function model_different_field_created_null_both()
  {
    $user = UserWithAnotherPhoneField::factory()->create(['home_phone' => null,'work_phone'=>null]);
    $this->assertNull($user->home_phone);
    $this->assertNull($user->work_phone);
  }


  /** @test */
  function model_different_field_created_valid()
  {
    $user = UserWithAnotherPhoneField::factory()->create();
    $this->assertTrue($user->home_phone instanceof PhoneNumber);
    $this->assertTrue($user->work_phone instanceof PhoneNumber);
  }



  /** @test */
  function model_different_field_created_from_existing_invalid()
  {
    \DB::insert('insert into users_alt (id,name, email,password,home_phone,home_phone_country) values (?,?, ?,?,?,?)', [999, 'Dayle', 'dayle@example.com', 'test', 'crap_number', 'ch']);
    //$user = User::factory()->create(['phone'=>'asdghasd','phone_country'=>'ch']);
    $user = UserWithAnotherPhoneField::find(999);
    $this->assertNotNull($user);
    $this->assertEquals($user->home_phone, 'crap_number');

    $user->home_phone = '+41227771277';
    $user->save();
    $this->assertTrue($user->home_phone instanceof PhoneNumber);
    $this->assertNotNull($user->home_phone_national);
    $this->assertNotNull($user->home_phone_normalized);
    $this->assertEquals($user->home_phone_normalized,'+41227771277');
    $this->assertEquals($user->home_phone_national,'022 777 12 77');

    $this->assertEquals(1, UserWithAnotherPhoneField::wherePhone('022 777','home_phone')->count());
    $this->assertEquals(1, UserWithAnotherPhoneField::wherePhone('22 777','home_phone')->count());


    $this->assertEquals(0, UserWithAnotherPhoneField::wherePhone('22 777','work_phone')->count());

    $user->work_phone = '+41227771277';
    $user->save();
    $this->assertTrue($user->work_phone instanceof PhoneNumber);

    $this->assertEquals(1, UserWithAnotherPhoneField::wherePhone('22 777','work_phone')->count());

  }



}
