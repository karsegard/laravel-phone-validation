<?php

namespace KDA\Laravel\PhoneValidation\Facades;

use Illuminate\Support\Facades\Facade;

class PhoneManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
