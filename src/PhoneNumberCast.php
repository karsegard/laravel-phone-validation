<?php
namespace KDA\Laravel\PhoneValidation;

use InvalidArgumentException;
use Propaganistas\LaravelPhone\Casts\RawPhoneNumberCast;
use Propaganistas\LaravelPhone\PhoneNumber;

class PhoneNumberCast extends RawPhoneNumberCast
{
     public function get($model, string $key, $value, array $attributes)
    {
        if (! $value) {
            return null;
        }

        $phone = new PhoneNumber($value,
            $this->getPossibleCountries($key, $attributes)
        );

        $country = $phone->getCountry();

        if ($country === null) {
       /*     throw new InvalidArgumentException('Missing country specification for '.$key.' attribute cast');*/
            return $value;
        }

        return new PhoneNumber($value, $country);
    }

    public function serialize($model, string $key, $value, array $attributes)
    {
        if (! $value) {
            return null;
        }
        if(is_string($value)){
            return $value;
        }

        return $value->getRawNumber();
    }
}