<?php
namespace KDA\Laravel\PhoneValidation\Enums;



enum PhoneValidationStatus: string
{
    case DRAFT = 'draft';
    case VALID = 'valid';
    case NOT_VALID = 'not_valid';
    case CONFIRMED = 'confirmed'; //for sms validation

    public static function toOptions(): array
    {
        return collect(self::cases())->mapWithKeys(fn ($case) => [$case->value=>$case->label()])->toArray();
    }

    public function label(){
        return __('laravel-phone-validation::phone_validation_status.'.$this->name);
    }

}


