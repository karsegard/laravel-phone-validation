<?php
namespace KDA\Laravel\PhoneValidation;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\PhoneValidation\Enums\PhoneValidationStatus;
use KDA\Laravel\Traits\HasCommands;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\PhoneValidation\Facades\PhoneManager as Facade;
use KDA\Laravel\PhoneValidation\PhoneManager as Library;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\ProvidesBlueprint;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;
    use ProvidesBlueprint;
    protected $packageName ='laravel-phone-validation';
    

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
     // trait \KDA\Laravel\Traits\HasConfig; 
     //    registers config file as 
     //      [file_relative_to_config_dir => namespace]
    protected $configDir='config';
    protected $configs = [
         'kda/phone-validation.php'  => 'kda.laravel-phone-validation'
    ];
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
        $this->blueprints = [
            'phone'=> function($field="phone",$default_status=PhoneValidationStatus::DRAFT,$default_country="ch"){
                $this->string("{$field}")->nullable();
                $this->string("{$field}_status")->default($default_status->value);
                $this->string("{$field}_country")->default($default_country)->nullable();
            },
            'phone_index'=>function($field="phone"){
                $this->string("{$field}_normalized")->nullable();
                $this->string("{$field}_national")->nullable();
            }
        ];
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
