<?php

namespace KDA\Laravel\PhoneValidation\Models\Traits;

use KDA\Laravel\PhoneValidation\Enums\PhoneValidationStatus;
use KDA\Laravel\PhoneValidation\PhoneNumberCast;
use Propaganistas\LaravelPhone\Casts\RawPhoneNumberCast;
trait HasPhoneNumbers
{
    //public ?array $phone_fields = null;
    public $phone_index = true;

    public static function bootHasPhoneNumbers(): void
    {
        static::creating(function ($model) {
            if ($model->phone_index) {
                foreach ($model->getPhoneFields() as $field) {
                    if ($model->isDirty($field) && $model->$field) {
                        $model->{$field . "_normalized"} = preg_replace('[^0-9]', '', $model->$field);
                        $model->{$field . "_national"} = preg_replace('[^0-9]', '', phone($model->$field, $model->{$field . "_country"})->formatNational());

                        if(!blank($model->$field) && $model->status!= PhoneValidationStatus::CONFIRMED){
                            $model->{$field."_status"} = PhoneValidationStatus::VALID;
                        }
                    }
                }
            }
        });
        static::updating(function ($model) {
            if ($model->phone_index) {
                foreach ($model->getPhoneFields() as $field) {
                    if ($model->isDirty($field) && $model->$field) {
                        $model->{$field . "_normalized"} = preg_replace('[^0-9]', '', $model->$field);
                        $model->{$field . "_national"} = preg_replace('[^0-9]', '', phone($model->$field, $model->{$field . "_country"})->formatNational());

                        if(!blank($model->$field) && $model->status!= PhoneValidationStatus::CONFIRMED){
                            $model->{$field."_status"} = PhoneValidationStatus::VALID;
                        }
                    }
                }
            }
        });
    }
    public function getPhoneFields(){
        return isset($this->phone_fields) ? $this->phone_fields : ['phone'] ;
    }

    public function initializeHasPhoneNumbers(): void
    {
        foreach ($this->getPhoneFields() as $field) {
            $this->fillable[] = $field;
            $this->fillable[] = "{$field}_status";
            $this->fillable[] = "{$field}_country";
            $this->casts["{$field}_status"] = PhoneValidationStatus::class;
            $this->casts["{$field}"] = PhoneNumberCast::class . ':country_field';
        }
       
    }

    public function scopeWherePhone($q, $value, $field = "phone")
    {
        return $q->where(function ($q) use($field,$value){
            $q->orWhere("{$field}_normalized", 'LIKE', '%'.preg_replace('[^0-9]', '', $value) . '%')
                ->orWhere("{$field}_national", 'LIKE', '%'.preg_replace('[^0-9]', '', $value) . '%');
        });
    }
}
